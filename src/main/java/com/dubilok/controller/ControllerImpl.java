package com.dubilok.controller;

import com.dubilok.model.bean_collections.Config;
import com.dubilok.model.bean_collections.GeneralBean;
import com.dubilok.model.beans_atowired.BeanRezA;
import com.dubilok.model.beans_atowired.BeanRezB;
import com.dubilok.model.beans_atowired.BeanRezC;
import com.dubilok.model.beans_atowired.BeanRezD;
import com.dubilok.model.profile.ConfigDev;
import com.dubilok.model.profile.ConfigProd;
import com.dubilok.model.profile.DataBase;
import com.dubilok.model.task_8.BeanTest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.AbstractEnvironment;

public class ControllerImpl implements Controller {

    private static Logger logger = LogManager.getLogger(ControllerImpl.class);

    @Override
    public void showTaskSeventh() {
        ApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        context.getBean(GeneralBean.class).printPhones();
    }

    @Override
    public void showTaskEight() {
        ApplicationContext context = new AnnotationConfigApplicationContext(com.dubilok.model.task_8.Config.class);
        logger.info(context.getBean(BeanTest.class));
    }

    @Override
    public void showTaskNigth() {
        ApplicationContext context = new AnnotationConfigApplicationContext(com.dubilok.model.beans_atowired.Config.class);
        logger.info(context.getBean(BeanRezA.class).toString() + " " + context.getBean(BeanRezA.class).hashCode());
        logger.info(context.getBean(BeanRezB.class).toString() + " " + context.getBean(BeanRezB.class).hashCode());
        logger.info(context.getBean(BeanRezA.class).toString() + " " + context.getBean(BeanRezA.class).hashCode());
        logger.info(context.getBean(BeanRezB.class).toString() + " " + context.getBean(BeanRezB.class).hashCode());
        logger.info(context.getBean(BeanRezC.class).toString() + " " + context.getBean(BeanRezC.class).hashCode());
        logger.info(context.getBean(BeanRezD.class).toString() + " " + context.getBean(BeanRezD.class).hashCode());
        logger.info(context.getBean(BeanRezC.class).toString() + " " + context.getBean(BeanRezC.class).hashCode());
        logger.info(context.getBean(BeanRezD.class).toString() + " " + context.getBean(BeanRezD.class).hashCode());
    }

    @Override
    public void useDevProfile() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.getEnvironment().setActiveProfiles("dev");
        context.register(ConfigDev.class);
        context.refresh();
        DataBase bean = context.getBean(DataBase.class);
        logger.info(bean);
    }

    @Override
    public void useProdProfile() {
        System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, "prod");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConfigProd.class);
        DataBase bean = context.getBean(DataBase.class);
        logger.info(bean);
    }
}
