package com.dubilok.model.beans_atowired;

import com.dubilok.model.other_beans.OtherBeanC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanRezC {
    private OtherBeanC otherBeanC;

    public BeanRezC() {
    }

    public OtherBeanC getOtherBeanC() {
        return otherBeanC;
    }

    @Autowired
    public void setOtherBeanC(OtherBeanC otherBeanC) {
        this.otherBeanC = otherBeanC;
    }

    @Override
    public String toString() {
        return "BeanRezC{" +
                "otherBeanC=" + otherBeanC +
                '}';
    }
}
