package com.dubilok.model.beans_atowired;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.dubilok.model.beans_atowired")
@ComponentScan("com.dubilok.model.other_beans")
public class Config {
}
