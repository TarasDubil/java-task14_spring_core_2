package com.dubilok.model.beans_atowired;

import com.dubilok.model.other_beans.OtherBeanA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanRezA {
    @Autowired
    private OtherBeanA otherBeanA;

    public BeanRezA() {
    }

    @Override
    public String toString() {
        return "BeanRezA{" +
                "otherBeanA=" + otherBeanA +
                '}';
    }
}
