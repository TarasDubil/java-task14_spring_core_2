package com.dubilok.model.beans_atowired;

import com.dubilok.model.other_beans.OtherBeanC;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class BeanRezD {
    @Qualifier("otherBeanC")
    private OtherBeanC otherBeanC;

    public BeanRezD() {
    }

    @Override
    public String toString() {
        return "BeanRezD{" +
                "otherBeanC=" + otherBeanC +
                '}';
    }
}
