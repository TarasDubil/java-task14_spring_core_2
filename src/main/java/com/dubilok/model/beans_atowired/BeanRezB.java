package com.dubilok.model.beans_atowired;

import com.dubilok.model.other_beans.OtherBeanB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanRezB {
    private OtherBeanB otherBeanB;

    public BeanRezB() {
    }

    @Autowired
    public BeanRezB(OtherBeanB otherBeanB) {
        this.otherBeanB = otherBeanB;
    }

    @Override
    public String toString() {
        return "BeanRezB{" +
                "otherBeanB=" + otherBeanB +
                '}';
    }
}
