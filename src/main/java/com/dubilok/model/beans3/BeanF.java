package com.dubilok.model.beans3;

import org.springframework.stereotype.Component;

@Component
public class BeanF {
    private String name;

    public BeanF(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "BeanF{" +
                "name='" + name + '\'' +
                '}';
    }
}
