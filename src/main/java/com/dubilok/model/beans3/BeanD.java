package com.dubilok.model.beans3;

import org.springframework.stereotype.Component;

@Component
public class BeanD {
    private String name;

    public BeanD(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "BeanD{" +
                "name='" + name + '\'' +
                '}';
    }
}
