package com.dubilok.model.beans3;

import org.springframework.stereotype.Component;

@Component
public class BeanE {
    private String name;

    public BeanE(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                '}';
    }
}
