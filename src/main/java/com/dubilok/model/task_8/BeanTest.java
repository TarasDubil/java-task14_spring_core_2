package com.dubilok.model.task_8;

import com.dubilok.model.bean_collections.Phone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class BeanTest {
    @Autowired
    @Qualifier("meizu")
    private Phone phone;

    @Autowired
    private Phone phoneSecond;

    public BeanTest() {
    }

    @Override
    public String toString() {
        return "BeanTest{" +
                "phone=" + phone.getPhone() +
                ", phoneSecond=" + phoneSecond.getPhone() +
                '}';
    }
}
