package com.dubilok.model.task_8;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.dubilok.model.task_8")
@ComponentScan("com.dubilok.model.bean_collections")
public class Config {
}
