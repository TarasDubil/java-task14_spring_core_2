package com.dubilok.model.beans1;

import org.springframework.stereotype.Component;

@Component
public class BeanC {

    private String name;

    public BeanC(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                '}';
    }
}
