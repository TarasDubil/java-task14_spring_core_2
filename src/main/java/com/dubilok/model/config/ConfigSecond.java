package com.dubilok.model.config;

import com.dubilok.model.beans3.BeanD;
import com.dubilok.model.beans3.BeanF;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.dubilok.model.beans2")
@ComponentScan(basePackageClasses = BeanD.class)
@ComponentScan(basePackageClasses = BeanF.class)
public class ConfigSecond {

}
