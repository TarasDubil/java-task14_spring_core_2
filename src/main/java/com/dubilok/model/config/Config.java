package com.dubilok.model.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.dubilok.model.beans1")
public class Config {

}
