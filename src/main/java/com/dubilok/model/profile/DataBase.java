package com.dubilok.model.profile;

public class DataBase {
    private String name;
    private String password;

    public DataBase(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "DataBase{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
