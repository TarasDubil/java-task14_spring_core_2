package com.dubilok.model.profile;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.AbstractEnvironment;

public class ProdMain {
    public static void main(String[] args) {
        System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, "prod");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConfigProd.class);
        System.out.println(context.getBean(DataBase.class));
    }
}
