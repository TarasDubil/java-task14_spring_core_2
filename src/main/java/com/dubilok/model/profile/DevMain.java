package com.dubilok.model.profile;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class DevMain {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.getEnvironment().setActiveProfiles("dev");
        context.register(ConfigDev.class);
        context.refresh();
        System.out.println(context.getBean(DataBase.class));
    }
}
