package com.dubilok.model.profile;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("prod")
@ComponentScan("com.dubilok.model.profile")
public class ConfigProd {
    @Bean
    public DataBase getDataBase() {
        String name = "//localhost:4105/lviv_db";
        String password = "*******";
        return new DataBase(name, password);
    }
}
