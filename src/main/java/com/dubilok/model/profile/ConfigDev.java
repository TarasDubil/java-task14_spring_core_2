package com.dubilok.model.profile;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan("com.dubilok.model.profile")
@Profile("dev")
public class ConfigDev {
    @Bean
    public DataBase getDataBase() {
        String name = "//localhost:4105/test_db";
        String password = "root";
        return new DataBase(name, password);
    }
}
