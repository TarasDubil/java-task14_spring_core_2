package com.dubilok.model.other_beans;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class OtherBeanA {
    private String name;

    public OtherBeanA() {
    }

    public OtherBeanA(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "OtherBeanA{" +
                "name='" + name + '\'' +
                '}';
    }
}
