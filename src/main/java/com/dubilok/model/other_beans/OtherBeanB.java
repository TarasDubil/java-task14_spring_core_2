package com.dubilok.model.other_beans;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class OtherBeanB {
    private String name;

    public OtherBeanB() {
    }

    public OtherBeanB(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "OtherBeanB{" +
                "name='" + name + '\'' +
                '}';
    }
}
