package com.dubilok.model.other_beans;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class OtherBeanC {
    private String name;

    public OtherBeanC() {
    }

    public OtherBeanC(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "OtherBeanC{" +
                "name='" + name + '\'' +
                '}';
    }
}
