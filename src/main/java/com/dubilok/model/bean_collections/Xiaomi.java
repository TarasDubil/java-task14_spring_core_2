package com.dubilok.model.bean_collections;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(3)
@Qualifier("myPhone")
public class Xiaomi implements Phone {

    public Xiaomi() {
    }

    @Override
    public String getPhone() {
        return "Xiaomi";
    }
}
