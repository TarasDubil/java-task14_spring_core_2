package com.dubilok.model.bean_collections;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.dubilok.model.bean_collections")
public class Config {
}
