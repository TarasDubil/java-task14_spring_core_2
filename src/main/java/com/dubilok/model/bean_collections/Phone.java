package com.dubilok.model.bean_collections;

public interface Phone {
    String getPhone();
}
