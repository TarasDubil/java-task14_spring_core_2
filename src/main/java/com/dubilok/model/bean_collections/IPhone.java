package com.dubilok.model.bean_collections;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
@Primary
@Qualifier("myPhone")
public class IPhone implements Phone {

    public IPhone() {
    }

    @Override
    public String getPhone() {
        return "Iphone";
    }
}
