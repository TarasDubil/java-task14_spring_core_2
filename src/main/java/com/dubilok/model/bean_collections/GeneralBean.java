package com.dubilok.model.bean_collections;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GeneralBean {

    private static Logger logger = LogManager.getLogger(GeneralBean.class);

    @Autowired
    @Qualifier("myPhone")
    private List<Phone> phones;

    public void printPhones() {
        phones.forEach(p -> logger.info(p.getPhone()));
    }
}
