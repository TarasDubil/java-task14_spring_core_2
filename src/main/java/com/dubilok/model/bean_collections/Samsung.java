package com.dubilok.model.bean_collections;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
@Qualifier("myPhone")
public class Samsung implements Phone {

    public Samsung() {
    }

    @Override
    public String getPhone() {
        return "Samsung";
    }
}
