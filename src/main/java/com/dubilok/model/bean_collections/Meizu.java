package com.dubilok.model.bean_collections;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("meizu")
public class Meizu implements Phone {

    public Meizu() {
    }

    @Override
    public String getPhone() {
        return "Meizu";
    }
}
