package com.dubilok.model.beans2;

import org.springframework.stereotype.Component;

@Component
public class NarcissusFlower {
    private String name;

    public NarcissusFlower(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "NarcissusFlower{" +
                "name='" + name + '\'' +
                '}';
    }
}
